<?php

namespace Drupal\datatables_cdn\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to enable datatables plugin.
 *
 * @Filter(
 *   id = "datatables",
 *   title = @Translation("Datatables"),
 *   description = @Translation("Render tables with the `datatables` class using datatables plugin."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class DataTables extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    if (strpos($text, 'dataTable') !== FALSE) {
      $dom = Html::load($text);
      $result->setProcessedText(Html::serialize($dom))
        ->addAttachments([
          'library' => [
            'datatables_cdn/datatables',
          ],
        ]);
    }

    return $result;
  }

}
