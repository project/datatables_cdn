<?php

namespace Drupal\datatables_cdn\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginContextualInterface;
use Drupal\ckeditor\CKEditorPluginInterface;
use Drupal\editor\Entity\Editor;


/**
 * Defines the "Datatables" plugin.
 *
 * @CKEditorPlugin(
 *   id = "datatables",
 *   label = @Translation("Adds `datatables` option to Table Properties."),
 *   module = "datatables_cdn"
 * )
 *
 * @codeCoverageIgnore
 */
class Datatables extends CKEditorPluginBase implements CKEditorPluginInterface, CKEditorPluginContextualInterface {

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return dirname(__DIR__, 4) . '/js/plugins/datatables/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(Editor $editor) {
    if (!$editor->hasAssociatedFilterFormat()) {
      return FALSE;
    }

    // Automatically enable this plugin if the text format associated with this
    // text editor uses the media_embed filter.
    $filters = $editor->getFilterFormat()->filters();
    return $filters->has('datatables') && $filters->get('datatables')->status;
  }


}
