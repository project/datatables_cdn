# Datatables CDN

This module provides a javascript library for using the Datatables plugin as 
well as associated tools.

See [https://datatables.net/](https://datatables.net/).

## INSTALLATION

The module can be installed via the
[standard Drupal installation process](http://drupal.org/node/895232).

## CONFIGURATION

If you would like to be able to convert tables from a text format, enable the 
Datatables filter for that format, then under `Table Properties` select the 
`Display as DataTable` option.
