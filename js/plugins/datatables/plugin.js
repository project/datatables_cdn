/**
 * @file
 * datatables plugin.
 */

(function ($, Drupal, CKEDITOR) {
  "use strict";

  CKEDITOR.plugins.add("datatables", {
    requires: ["dialog", "table"],
    init: function (editor) {
      if (editor.blockless) return;

      CKEDITOR.on('dialogDefinition', function( ev ) {
        var dialogName = ev.data.name;
        if ( dialogName === 'tableProperties' ) {
          var dialogDefinition = ev.data.definition;
          var tableProperties = dialogDefinition.getContents( 'info' );
          tableProperties.add({
            type: 'checkbox',
            id: 'datatable',
            label: 'Display as DataTable',
            'default': false,
          });

          // Add our own onOK logic, then trigger original.
          var onOk = dialogDefinition.onOk;
          dialogDefinition.onOk = function( e ) {
            var table = editor.elementPath().contains("table", 1);
            var input = this.getContentElement( 'info', 'datatable' );
            if (table && input.getValue()) {
              table.$.classList.add('dataTable');
            }
            else {
              table.$.classList.remove('dataTable');
            }
            onOk && onOk.apply( this, e );
          };

          // Add our own onShow logic, then trigger original.
          var onShow = dialogDefinition.onShow;
          dialogDefinition.onShow = function( e ) {
            var table = editor.elementPath().contains("table", 1);
            var input = this.getContentElement( 'info', 'datatable' );
            if (table.$.classList.contains('dataTable')) {
              input.setValue(true);
            }
            else {
              input.setValue(false);
            }
            onShow && onShow.apply( this, e );
          };
        }
      });
    },
  });

})(jQuery, Drupal, CKEDITOR);
